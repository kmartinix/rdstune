require_relative 'lib/version'

Gem::Specification.new do |s|
  s.name        = 'rdstune'
  s.version     = VERSION
  s.date        = '2016-02-11'
  s.summary     = "Creates Tuned RDS Parameter Group for Postgres"
  s.description = "Generates a useful base configuration for PostgreSQL in RDS"
  s.authors     = ["David Kerr"]
  s.email       = 'dave@davidmkerr.com'
  s.files       = [ "lib/rdstune.rb",
                    "lib/version.rb",
                    "bin/rdstune",
                    "license.txt",
                    "contributors.txt"]
  s.homepage    = 'https://bitbucket.org/davidkerr/rdstune'
  s.license     = 'BSD'
  s.add_runtime_dependency 'aws-sdk','~>2.0'
  s.add_runtime_dependency 'trollop','~>2.0'
  s.add_runtime_dependency 'mrtuner','~>0.5'
  s.executables << 'rdstune'
end
